CREATE TABLE `article_topic_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
  `topic_id` INT UNSIGNED NOT NULL COMMENT '话题id',
  PRIMARY KEY `id_pk` (`id`),
  UNIQUE INDEX `ids1_uq` (`article_id`, `topic_id`),
  INDEX `articleId_index` (`article_id`),
  INDEX `topicId_index` (`topic_id`)
) ENGINE=MYISAM COMMENT='文章话题表' CHARSET=utf8 COLLATE=utf8_general_ci;